#!/bin/env python3
from bs4 import BeautifulSoup
import urllib.request
import os
import csv
import json

columns = ["start-month", "end-month", "start-issue",
           "end-issue", "title", "authors", "subjects", "type"]

cachefolder = "cache"

debugging = os.environ.get('DEBUG') is not None


def debug(message):
    if debugging:
        print(message)


def readUrl(url):
    debug("fetching: " + url)
    return urllib.request.urlopen(url).read()


def cacheName(url):
    fname = os.path.basename(url)
    return os.path.join(cachefolder, fname)


def putInCache(url, content):
    filename = cacheName(url)
    with open(filename, 'wb') as f:
        return f.write(content)


def getFromCache(url):
    try:
        filename = cacheName(url)
        with open(filename, 'r', errors='ignore') as f:
            return f.read()
    except FileNotFoundError:
        return None


def get(url):
    if not url.startswith('http'):
        url = f"http://www.mfcindia.org/mfcpdfs/{url}"
    cache = getFromCache(url)
    if cache is not None:
        return cache
    # no cache. Download and cache and return
    fromUrl = readUrl(url)
    putInCache(url, fromUrl)
    return fromUrl


def getSoup(url):
    html_doc = get(url)
    return BeautifulSoup(html_doc, 'html.parser')


months = {
    'Jan': '01',
    'Feb': '02',
    'Mar': '03',
    'Apr': '04',
    'May': '05',
    'Jun': '06',
    'Jul': '07',
    'Aug': '08',
    'Sep': '09',
    'Oct': '10',
    'Nov': '11',
    'Dec': '12',
}


def getOneMonth(month):
    month = month.strip()
    if month == "":
        debug("Suspected special issue")
        return "10"
    abbr = month[0:3]
    return months[abbr]


def getYearAndMonthFromMonthYear(month):
    [monthPart, year] = month.split(' ')
    return '20' + year + '-' + getOneMonth(monthPart)


def getMonthFromMonthYear(months):
    if '-' in months:
        [start, end] = months.split('-')
        endvalue = getYearAndMonthFromMonthYear(end.strip())
        try:
            # This startvalue endvalue contraption is to capture
            # columns like  Feb-July 11
            startvalue = getYearAndMonthFromMonthYear(start.strip())
            return [startvalue, endvalue]
        except ValueError:
            startvalue = endvalue[0:5] + getOneMonth(start)
            return [startvalue, endvalue]
    else:
        return [
            getYearAndMonthFromMonthYear(months.strip()),
            getYearAndMonthFromMonthYear(months.strip())
        ]


def getStartEndMonth(year, months):
    if any(char.isdigit() for char in months):
        return getMonthFromMonthYear(months)
    startyear = year
    endyear = year
    if '-' in year:
        startyear = year.split('-')[0]
        endyear = year.split('-')[1]
    if '-' in months:
        split = months.split('-')
        return [startyear + '-' + getOneMonth(split[0]), endyear + '-' + getOneMonth(split[1])]
    else:
        return [startyear + '-' + getOneMonth(months), endyear + '-' + getOneMonth(months)]


def getIssues(issue):
    answer = []
    if '-' in issue:
        answer = issue.split('-')
    elif 'to' in issue:
        answer = issue.split('to')
    else:
        answer = [issue, issue]
    answer = [x.strip() for x in answer]
    return answer


def clean(title):
    return title.replace('\n', ' ').strip()


def split_authors(a):
    replaced = a.replace(',', '#').replace('&', '#').replace('and', '#')
    split = replaced.split('#')
    cleaned = [x.strip().replace('/', '\\') for x in split]
    return cleaned


def processRow(row):
    data = {}
    months = getStartEndMonth(row[0].get_text(), row[1].get_text())
    data['startMonth'] = months[0]
    if '205' in data['startMonth']:
        data['startMonth'] = data['startMonth'].replace('205', '2005')
    data['endMonth'] = months[1]
    if '206' in data['endMonth']:
        data['endMonth'] = data['endMonth'].replace('206', '2006')
    issues = getIssues(row[2].get_text())
    data['startIssue'] = issues[0]
    data['endIssue'] = issues[1]
    if data['startIssue'] == data['endIssue']:
        data['issueName'] = data['startIssue']
    else:
        data['issueName'] = data['startIssue'] + '-' + data['endIssue']
    data['title'] = clean(row[3].get_text())
    debug(data['title'])
    a = row[3].find('a')
    if a is None:
        a = row[2].find('a')
    if a is not None:
        data['originalFileLink'] = a.get('href')
    data['authors'] = split_authors(row[4].get_text())
    if len(row) > 5:
        data['subject'] = clean(row[5].get_text())
    if len(row) > 6:
        data['type'] = clean(row[6].get_text())
    return data


def processSoup(soup):
    rows = soup.find('tbody').findAll('tr', recursive=False)
    skipped = False
    data = []
    for row in rows:
        if not skipped:
            skipped = True
            continue
        cols = row.findAll('td', recursive=False)
        processed = processRow(cols)
        data.append(processed)
    return data


def write_content(name, content):
    path = os.path.join('site', 'content', 'item', name)
    with open(path, 'w') as f:
        f.write(content)

def getName(url):
    pdfname = os.path.basename(url)
    onlyname = os.path.splitext(pdfname)[0]
    return onlyname

def getText(url):
    if (url == ''):
        return ''
    onlyname = getName(url)
    text = os.path.join(cachefolder, onlyname + ".txt")
    try:
        with open(text, 'r') as f:
            return f.read()
    except FileNotFoundError:
        print(f"{onlyname} is missing")
        return ""

def getSlug(row):
    return row['issueName'] + ": " + row['title'].replace(' ', '-').replace('/', '-').lower()

b70s = getSoup('http://www.mfcindia.org/mfcpdfs/70s.html')
b80s = getSoup('http://www.mfcindia.org/mfcpdfs/80s.html')
b90s = getSoup('http://www.mfcindia.org/mfcpdfs/90s.html')
b00s = getSoup('http://www.mfcindia.org/mfcpdfs/2000s.html')
b01s = getSoup('http://www.mfcindia.org/mfcpdfs/2011-.html')

if __name__ == '__main__':
    rows = []
    for i in processSoup(b70s):
        rows.append(i)
    for i in processSoup(b80s):
        rows.append(i)
    for i in processSoup(b90s):
        rows.append(i)
    for i in processSoup(b00s):
        rows.append(i)
    for i in processSoup(b01s):
        rows.append(i)
    with open('output.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([
            'original_link',
            'start_month',
            'end_month',
            'issue_name',
            'title',
            'authors',
            'subject',
            'article_type'
        ])
        for row in rows:
            towrite = []
            towrite.append(row.get('originalFileLink', ''))
            towrite.append(row['startMonth'])
            towrite.append(row['endMonth'])
            towrite.append(row['issueName'])
            towrite.append(row['title'])
            towrite.append(';'.join(row['authors']))
            towrite.append(row['subject'])
            towrite.append(row.get('type', ''))
            writer.writerow(towrite)

            content = json.dumps({
                'startMonth': row['startMonth'],
                'endMonth': row['endMonth'],
                'issueName' : row['issueName'],
                'title' : row['title'],
                'authors' : row['authors'],
                'subject' : row.get('subject', ''),
                'type' : row.get('type', ''),
                'originalFileLink': row.get('originalFileLink', '')
            })

            content += "\n\n" + getText(row.get('originalFileLink', ''))
            name = getSlug(row) + '.md'
            write_content(name, content)