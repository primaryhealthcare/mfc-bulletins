import unittest
import process

class TestProcessor(unittest.TestCase):

    def test_month(self):
        self.assertEqual(process.getOneMonth('Sept.'), '09')

    def test_getStartEndMonth(self):
        self.assertEqual(process.getStartEndMonth('Nov.-Dec.'), ['11', '12'])
        self.assertEqual(process.getStartEndMonth('April'), ['04', '04'])

if __name__ == '__main__':
    unittest.main()
